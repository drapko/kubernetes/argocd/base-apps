# Cluster installation

[[_TOC_]]

## Controlplane nodes

One master node:

```bash
sudo kubeadm init --apiserver-advertise-address=192.168.89.151 --upload-certs
```

Multiple master nodes:

```bash
sudo kubeadm init --control-plane-endpoint "LB IP/FQDN:port" --upload-certs
```

Example:

```bash
sudo kubeadm init --control-plane-endpoint "k8snlvapi.home:6443" --upload-certs
```

Follow instructions to add more controlplane nodes.

If needed, set pod CIDR network: `--pod-network-cidr 10.244.0.0/16`

## Join worker nodes

If you need to join in the first 2 hours after first master node installation, you must create and upload new certs.

Steps:

```bash
sudo kubeadm init phase upload-certs --upload-certs

sudo kubeadm token create --print-join-command --certificate-key <key>
```

Last command will generate `kubeadm join` command valid for master and worker nodes.

Both node types need:

* `--token`
* `--discovery-token-ca-cert-hash`

Master node types need additional parameters:

* `--control-plane`
* `--certificate-key`

Master node join example:

```bash
sudo kubeadm join LB:IP --token dxs0fr.2ac1zigj5qh2hl4q --discovery-token-ca-cert-hash \
  sha256:1f004f0e9c1ec52dc188a70445d01268cced9017ca1cba2d519a84153c108de7 --control-plane \
  --certificate-key d6332e56582dbce099d70e9d58760f21c3a71b8579e28e1b305e5fa4ba13720e
```

## Hashicorp Vault: Using JWT validation public keys

Official documentation for [using JWT validation public keys](https://developer.hashicorp.com/vault/docs/auth/jwt/oidc-providers/kubernetes#using-jwt-validation-public-keys)

### Option 1:

1. Get the service account public key from one of the master nodes.

```bash
cat /etc/kubernetes/pki/sa.pub
```

### Option 2:

1. Fetch the service account signing public key from your cluster's JWKS URI.

```bash
kubectl get --raw "$(kubectl get --raw /.well-known/openid-configuration | jq -r '.jwks_uri' | sed -r 's/.*\.[^/]+(.*)/\1/')"
```

2. Convert the keys from JWK format to PEM. You can use a CLI tool or an online converter such as [this one](https://8gwifi.org/jwkconvertfunctions.jsp).
Must be pasted the "use" JSON object key (without the array).

### Final steps:

1. Use [Cyberchef](https://gchq.github.io/CyberChef/#recipe=Escape_string('Special%20chars','Single',false,true,false)) to escape this string.

2. Add it to Hashicorp Vault from my Terraform code repo (`auth.tf` file)

## CNI: cilium

We need to install any CNI before deploy any pod:

```bash
helm repo add cilium https://helm.cilium.io/

helm install cilium cilium/cilium --version 1.12.6 -n kube-system
```

# ArgoCD

* Install ArgoCD (changes done to work with Cilium CNI):

```bash
kubectl apply -k argocd
```

* Get ArgoCD admin password:

```bash
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
```

* Apply bootstrap file

 Apply the `apps` file according to your cluster for installing all resources that applications will use widely.
 
 Naming convention is `apps-environment`

```bash
kubectl apply -f apps-nonlive.yaml
```

## App of apps pattern

This repository uses ArgoCD app of apps pattern for cluster bootstrapping.

`apps-environment.yaml` files will reference to tag version of this own repository. This files must be applied with `kubectl` each time it's upgraded.

1. **code**: it must be upgraded, tagged and pushed
2. **kubectl**: it must be applied with the new tag